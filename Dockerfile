FROM existenz/webstack:8.0

RUN apk -U --no-cache add \
    openssl \
    libzip-dev \
    openssh-client \
    rsync \
    libxslt-dev \
    unzip \
    zip \
    iproute2 \
    mariadb-client \
    php-cli \
    php8-fileinfo \
    php8-ctype \
    php8-curl \
    php8-openssl \
    php8-json \
    php8-phar \
    php8-iconv \
    php8-mbstring \
    php8-sodium \
    php8-tokenizer \
    php8-xmlwriter \
    php8-simplexml \
    php8-dom \
    php8-xml \
    php8-mysqli

RUN ln -s /usr/bin/php8 /usr/bin/php

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY ./config/nginx.conf /etc/nginx/nginx.conf
